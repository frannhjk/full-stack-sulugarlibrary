﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SuLugar.Models.Identity;

namespace SuLugar.Controllers
{
    public class AccountController : Controller
    {
        // Traer clases usando Dependency Injection
        private UserManager<AppUser> UserMgr;
        private SignInManager<AppUser> SignMgr;

       public AccountController
            (UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            UserMgr = userManager;
            SignMgr = signInManager;    
        }

        // Log Out
        public async Task<IActionResult> Logout()
        {
            await SignMgr.SignOutAsync();

            return RedirectToAction("Index", "Home");
        }

        // Login
        public async Task<IActionResult> Login()
        {
            var result = await SignMgr.PasswordSignInAsync("testUser", "Test123!", false, false); // 123 seria el pw

            if (result.Succeeded)
            {
                return RedirectToAction("FullPage", "Home");
            }
            else
            {
                //return RedirectToAction("Index", "Home");
                return RedirectToAction("FullPage", "Home");
            }
        }

        // Register
        public async Task<IActionResult> Register()
        {
            try
            {
                ViewBag.Message = "El usuario ya está registrado";

                AppUser user = await UserMgr.FindByNameAsync("testUser");
                
                if(user == null)
                {
                    user = new AppUser();
                    user.UserName = "test";
                    user.Email = "test@test.com";
                    user.Firstname = "Franco";
                    user.Lastname = "Test";

                    IdentityResult result = await UserMgr.CreateAsync(user, "Test123!");

                    ViewBag.Message = "Usuario creado!!";
                }
                else
                {
                    ViewBag.Message = "usuario ya registrado";
                }
            }
            catch(Exception e)
            {
                ViewBag.Message = e.Message;
            }

            return View();
        }
    }
}