﻿using Microsoft.AspNetCore.Mvc;
using SuLugar.Interfaces;
using SuLugar.Models;
using SuLugar.ViewModels.Patron;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Controllers
{
    public class PatronController : Controller
    {
        private IPatron _patron;

        // CTR
        public PatronController(IPatron patron)
        {
            _patron = patron;
        }


        public IActionResult Index()
        {
            var allPatrons = _patron.GetAll(); 


            // ViewModel 1
            var patronModels = allPatrons
                .Select(patron => new PatronDetailModel
                {
                    Id = patron.Id,
                    Firstname = patron.FirstName ?? "No provisto",
                    Lastname = patron.LastName ?? "No provisto",
                    LibraryCardId = patron.LibraryCard.Id,
                    OverdueFees = patron.LibraryCard.Fees,
                    HomeLibrary = patron.HomeLibraryBranch.Name
                }).ToList();

            // ViewModel 2 list
            var model = new PatronIndexModel()
            {
                Patrons = patronModels
            };

            return View(model);
        }

        public IActionResult Detail(int id)
        {
            var patron = _patron.Get(id);
            try
            {
                var patronDetail = new PatronDetailModel()
                {
                    Lastname = patron.LastName,
                    Firstname = patron.FirstName,
                    Address = patron.Address,
                    HomeLibrary = patron.HomeLibraryBranch.Name,
                    MemberSince = patron.LibraryCard.Created,
                    OverdueFees = patron.LibraryCard.Fees,
                    LibraryCardId = patron.LibraryCard.Id,
                    Telephone = patron.Telephone,
                    AssetsCheckedOut = _patron.GetCheckouts(id).ToList() ?? new List<Checkout>(),
                    CheckoutHistory = _patron.GetCheckoutHistory(id),
                    Holds = _patron.GetHolds(id)
                };

                return View(patronDetail);
            }
            catch(Exception e)
            {
                throw e;
            }

            
        }

    }
}
