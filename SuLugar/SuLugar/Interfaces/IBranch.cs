﻿using SuLugar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Interfaces
{
    public interface IBranch
    {
        IEnumerable<LibraryBranch> GetAll();
        IEnumerable<Patro> GetPatrons(int branchId);
        IEnumerable<LibraryAsset> GetAssets(int branchId);
        IEnumerable<string> GetBranchHours(int branchId);
        LibraryBranch Get(int branchId);
        void Add(LibraryBranch newBranch);
        bool isBranchOpen(int branchId);

    }
}
