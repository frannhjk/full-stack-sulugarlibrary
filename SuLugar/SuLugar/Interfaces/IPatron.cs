﻿
using SuLugar.Models;
using System.Collections.Generic;

namespace SuLugar.Interfaces
{
    public interface IPatron
    {
        Patro Get(int id);
        IEnumerable<Patro> GetAll();
        void Add(Patro newPatron);

        IEnumerable<CheckoutHistory> GetCheckoutHistory(int patronId);
        IEnumerable<Hold> GetHolds(int patronId);
        IEnumerable<Checkout> GetCheckouts(int patronId);
    }
}
