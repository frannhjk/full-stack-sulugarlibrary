﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuLugar.Migrations
{
    public partial class updatebranchHoursclass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "BranchHourse",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BranchHourse_BranchId",
                table: "BranchHourse",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_BranchHourse_LibraryBranchs_BranchId",
                table: "BranchHourse",
                column: "BranchId",
                principalTable: "LibraryBranchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BranchHourse_LibraryBranchs_BranchId",
                table: "BranchHourse");

            migrationBuilder.DropIndex(
                name: "IX_BranchHourse_BranchId",
                table: "BranchHourse");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "BranchHourse");
        }
    }
}
