﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Models
{
    public class LibraryBranch
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30, ErrorMessage="Limitado a 30 caracteres.")]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Telephone { get; set; }
        public string Description { get; set; }
        public DateTime OpenDate { get; set; }

        public virtual IEnumerable<Patro> Patrons { get; set; } //Coleccion de Clientes
        public virtual IEnumerable<LibraryAsset> LibraryAssets { get; set; } //Colección de Assets

        public string ImageUrl { get; set; }

    }
}
