﻿using Microsoft.EntityFrameworkCore;
using SuLugar.Interfaces;
using SuLugar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Services
{
    public class BranchService : IBranch
    {
        private suLugarDbContext _context;

        public BranchService(suLugarDbContext context)
        {
            _context = context;
        }
        public void Add(LibraryBranch newBranch)
        {
            _context.Add(newBranch);
            _context.SaveChanges();
        }

        public LibraryBranch Get(int branchId)
        {
            return GetAll().FirstOrDefault(b => b.Id == branchId);
        }

        public IEnumerable<LibraryBranch> GetAll()
        {
            return _context.LibraryBranchs
                .Include(p => p.Patrons)
                .Include(ls => ls.LibraryAssets);
                
        }

        public IEnumerable<LibraryAsset> GetAssets(int branchId)
        {
            return _context.LibraryBranchs
                .Include(b => b.LibraryAssets)
                .FirstOrDefault(b => b.Id == branchId).LibraryAssets;
        }

        public IEnumerable<string> GetBranchHours(int branchId)
        {
            var hours = _context.BranchHourse.Where(h => h.Branch.Id == branchId);

            return DataHelper.FormatBusinessHours(hours);

        }

        public IEnumerable<Patro> GetPatrons(int branchId)
        {
            return _context.LibraryBranchs
                .Include(p => p.Patrons)
                .FirstOrDefault(b => b.Id == branchId)
                .Patrons;
        }

        public bool isBranchOpen(int branchId)
        {
            //var currentTimeHour = DateTime.Now.Hour;

            //var currentDay = (int)DateTime.Now.DayOfWeek + 1;

            //var hours = _context.BranchHourse.Where(h => h.Branch.Id == branchId);

            //var daysHours = hours.FirstOrDefault(h => h.DayOfWeek == currentDay);

            //if (currentTimeHour < daysHours.CloseTime && currentTimeHour > daysHours.OpenTime)
            //    return true;
            //else
            //    return false;

            return true;
        }
    }
}
