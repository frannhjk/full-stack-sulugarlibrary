﻿using SuLugar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Services
{
    public class DataHelper
    {
        public static IEnumerable<string> FormatBusinessHours(IEnumerable<BranchHours> branchHours)
        {
            var hours = new List<string>();

            // Recorro todos los valores del set branchHours 
            foreach(var time in branchHours)
            {
                // Formateo dia, openTime y closeTime
                var day = formatDay(time.DayOfWeek); 
                var openTime = formatTime(time.OpenTime);
                var closeTime = formatTime(time.CloseTime);

                var timeEntry = $"{day} {openTime} hasta {closeTime}";

                hours.Add(timeEntry);
            }

            return hours;
        }

        public static string formatDay(int number)
        {
            return Enum.GetName(typeof(DayOfWeek), number);
        }

        public static string formatTime(int time)
        {
            return TimeSpan.FromHours(time).ToString("hh':'mm");
        }
    }
}
