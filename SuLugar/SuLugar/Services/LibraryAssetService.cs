﻿using Microsoft.EntityFrameworkCore;
using SuLugar.Interfaces;
using SuLugar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Services
{
    public class LibraryAssetService : ILibraryAsset
    {
        private suLugarDbContext _context;

        public LibraryAssetService(suLugarDbContext context)
        {
            _context = context;
        }

        public void Add(LibraryAsset newAsset)
        {
            _context.Add(newAsset);
            _context.SaveChanges();
        }

        public IEnumerable<LibraryAsset> GetAll()
        {
            return _context.LibraryAssets
                .Include(asset => asset.Status)
                .Include(asset => asset.Location);
        }

        public LibraryAsset GetById(int id)
        {
            return _context.LibraryAssets
                .Include(asset => asset.Status)
                .Include(asset => asset.Location)
                .FirstOrDefault(asset => asset.Id == id);

            // o podria hacer return getAll().firstOrDefault(asset => asset.id == id);
        }

        public LibraryBranch GetCurrentLocation(int id)
        {
            //return _context.LibraryAssets.FirstOrDefault(asset => asset.Id == id).Location;
            return GetById(id).Location;
        }

        public string getDeweyIndex(int id)
        {
            // Discriminator
            // var isBook = _context.LibraryAssets.OfType<Book>().Where(a => a.id == id).Any();
            if (_context.Books.Any(book => book.Id == id))
            {
                return _context.Books
                    .FirstOrDefault(book => book.Id == id).DeweyIndex;
            }
            else
            {
                return "";
            }
        }

        public string GetIsbn(int id)
        {
            if (_context.Books.Any(book => book.Id == id))
            {
                return _context.Books
                    .FirstOrDefault(book => book.Id == id).ISBN;
            }
            else
                return "";
            
        }

        public string GetTitle(int id)
        {
            return GetById(id).Title;
        }

        public string GetType(int id)
        {
            var book = _context.LibraryAssets.OfType<Book>()
                .Where(asset => asset.Id == id);

            return book.Any() ? "Book" : "Video";
        }

        public string GetAuthorOrDirector(int id)
        {
            var isBook = _context.LibraryAssets.OfType<Book>()
                .Where(asset => asset.Id == id).Any();

            var isVideo = _context.LibraryAssets.OfType<Video>() // TODO: Refactor, innecesario, solo para prueba
                .Where(asset => asset.Id == id).Any();

            return isBook ?
                _context.Books.FirstOrDefault(book => book.Id == id).Author :
                _context.Videos.FirstOrDefault(vid => vid.Id == id).Director
                ?? "Desconocido";
        }
    }
}
