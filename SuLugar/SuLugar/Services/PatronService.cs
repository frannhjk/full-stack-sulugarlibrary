﻿using Microsoft.EntityFrameworkCore;
using SuLugar.Interfaces;
using SuLugar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar.Services
{
    public class PatronService : IPatron
    {
        private suLugarDbContext _context;


        public PatronService(suLugarDbContext context)
        {
            _context = context;
        }

        public void Add(Patro newPatron)
        {
            _context.Add(newPatron);
            _context.SaveChanges();
        }

        public Patro Get(int id)
        {
            // Refactor 
            return _context.Patrons
                .Include(patron => patron.LibraryCard)
                .Include(patron => patron.HomeLibraryBranch)
                .FirstOrDefault(patron => patron.Id == id);
            
            //return GetAll()
            //    .FirstOrDefault(patron => patron.Id == id);
        }

        public IEnumerable<Patro> GetAll()
        {
            return _context.Patrons
                .Include(patron => patron.LibraryCard)
                .Include(patron => patron.HomeLibraryBranch);
        }

        public IEnumerable<CheckoutHistory> GetCheckoutHistory(int patronId)
        {
            // Obtengo el cliente, luego incluyo la libraryCard y obtengo su id, todo para el cliente que recibo como parámetro
            var cardId = _context.Patrons
                .Include(patron => patron.LibraryCard)
                .FirstOrDefault(patron => patron.Id == patronId)
                .LibraryCard.Id;

            return _context.CheckoutHistories
                .Include(co => co.LibraryCard)
                .Include(co => co.LibraryAsset)
                .Where(co => co.LibraryCard.Id == cardId)
                .OrderByDescending(co => co.CheckedOut);
        }

        public IEnumerable<Checkout> GetCheckouts(int patronId)
        {
            // Refactor
            var cardId = Get(patronId).LibraryCard.Id;
            
            //var cardId = _context.Patrons
            //    .Include(patron => patron.LibraryCard)
            //    .FirstOrDefault(patron => patron.Id == patronId)
            //    .LibraryCard.Id;

            return _context.Checkouts
                .Include(co => co.LibraryCard)
                .Include(co => co.LibraryAsset)
                .Where(co => co.LibraryCard.Id == cardId);

        }
        public IEnumerable<Hold> GetHolds(int patronId)
        {
            var cardId = Get(patronId).LibraryCard.Id;

            return _context.Holds
                .Include(co => co.LibraryCard)
                .Include(co => co.LibraryAsset)
                .Where(co => co.LibraryCard.Id == cardId)
                .OrderByDescending(h => h.HoldPlaced);
        }
    }
}
