﻿using System;
using System.Collections.Generic;


namespace SuLugar.ViewModels.Catalog
{
    public class AssetIndexModel
    {
        public IEnumerable<AssetIndexListingModel> Assets { get; set; }
    }
}
