﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SuLugar.Models;
using SuLugar.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuLugar
{
    public class suLugarDbContext : IdentityDbContext<AppUser, AppRole, int>
    {
        //public suLugarDbContext(DbContextOptions options) : base(options) { }

        public suLugarDbContext(DbContextOptions<suLugarDbContext> options) : base(options)
        {

        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Checkout> Checkouts { get; set; }
        public DbSet<CheckoutHistory> CheckoutHistories { get; set; }
        public DbSet<LibraryBranch> LibraryBranchs { get; set; }
        public DbSet<BranchHours> BranchHourse { get; set; }
        public DbSet<LibraryCard> LibraryCards { get; set; }
        public DbSet<Patro> Patrons { get; set; }
        public DbSet<LibraryAsset> LibraryAssets { get; set; }
        public DbSet<Hold> Holds { get; set; }
        public DbSet<Status> Status { get; set; }
        public object Patro { get; internal set; }
    }
}
 

